import eng_to_ipa as ipa # a peculiarity: "us" is interpreted as U.S. with the corresponding pronunciation.
#suffixes of proper names that are translations of English common nouns
#or are used in international context (that is, the eng_to_ipa module can handle them)
# should probably be formed in the English way and not mangled, e.g. Japan -> Choe'bin, Japanese -> Chairba'nauf,
# not *Choe'binese. The derived word in Lojanese is created simply by running the derived word in English through the cipher

englishLetterTypes = [
["ɑ", "ɒ", "æ", "ɛ", "ɪ", "i", "o", "e", "ɔ", "ʊ", "a", "u", "ʌ", "ə", "ɜ"], #vowels "ɒ̃", "æ̃",
["j", "w"], #semivowels
["m", "n", "ŋ"], #nasals
["l", "r"], #flapsLaterals = ['l','r']
["ð", "f", "h", "s", "ʃ", "θ", "v", "z", "ʒ", "x"], #fricatives = ['c','f','h','q','s','v','z'] th
["b", "d", "g", "k", "p", "t"],  #plosives = ['b','d','g','k','p','t','x']
["ʔ", "ʤ", "ʧ"] #stops/affricates - reversed order for historical reasons (making "region" TL sound good)?
]

orthographyDict = { #basically taken from Wikipedia articles on English and IPA
    "b": "b",
    "d": "d",
    "ð": "th",
    "f": "f",
    "g": "g",
    "h": "h",
    "j": "y",
    "k": "k",
    "l": "l",
    "m": "m",
    "n": "n",
    "ŋ": "ng",
    "p": "p",
    "r": "r",
    "s": "s",
    "ʃ": "sh",
    "t": "t",
    "θ": "th",
    "v": "v",
    "w": "w",
    "z": "z",
    "ʒ": "zh",
    "ʤ": "j",
    "ʧ": "ch",

    "ː": "???",
    "a": "aa",
    "ɑ": "a",
    "ɒ": "oa", #??
    "æ": "ae",
    "ɛ": "air",
    "ɪ": "i",
    "i": "ea",
    "o": "o",
    "ɔ": "au",
    "ʊ": "uu", #ou? uu?
    "u": "ue",
    "ʌ": "u",
    "e": "e",

    "ə": "-", # ' mark

    "ˈ": "'",
    "ˌ": "",
    
    "x": "kh",
    "ʔ": "q", # no better English letter available
    "ɜ": "oe",
    
    "aʊ": "ao",
    "eɪ": "ay",
    "oʊ": "eau",
    "ɪə": "ee",
    "aɪ": "y",
    "ɔɪ": "oi",
    "ʊo": "uo",
    #"̃": "n", #nasalized vowel

    "": ""

}

def findNestedList(item, nestedList):
    for l in range(len(nestedList)):
        for it in range(len(nestedList[l])):
            if item == nestedList[l][it]:
                return (l, it)
    return (-1,-1)

def getShiftedItem(aList, pos, shift):
    newPos = (pos + shift) % len(aList)
    return aList[newPos]

def obfuscateLetter(letter, nestedList, shift):
        position = findNestedList(letter, nestedList)
        if position[0] == -1:
            return letter
        else:
            sublist = nestedList[position[0]]
            letterpos = position[1]
            return getShiftedItem(sublist, letterpos, shift)


def isVowel(letter):
    return (letter in englishLetterTypes[0]) #todo: long vowel mark


def obfuscateString(ipaStr, nestedList):
    newStr = []
    lastWasVowel = False
    syllableNum = 1
    #Increments on vowel group end. This isn't accurate for syllable codas but whatever.
    for i in range(len(ipaStr)):
        if ipaStr[i] == " ":
            syllableNum = 1
            lastWasVowel = False
        if isVowel(ipaStr[i]):
            lastWasVowel = True
        else:
            if lastWasVowel == True:
                syllableNum+=1
            lastWasVowel = False
        newStr.append(obfuscateLetter(ipaStr[i], nestedList, syllableNum))

            
    return "".join(newStr)

def ortPhone(phone, ortDict): #handles also strings of phones written as one letter
    if phone in ortDict:
        return ortDict[phone]
    else:
        #e.g. punctuation
        return phone
        
def ortPhoneGroup(phones, ortDict):
    if len(phones) == 0:
        return ""
    #process the longest prefix possible
    for i in range(len(phones), -1, -1):
        if phones[:i] in ortDict:
            if i != len(phones) and i != 0:
                #separate vowels visibly
                return ortDict[phones[:i]] + "'" + ortPhoneGroup(phones[i:], ortDict)
            else:
                return ortDict[phones[:i]] + ortPhoneGroup(phones[i:], ortDict)
    print("Error2")
    return phones
        

def ortographize(phonStr, ortDict): #parse vowel groups?
    vowelBuffer = []
    newStr = []
    for i in range(len(phonStr)):
        #print(phonStr[i],vowelBuffer)
        if isVowel(phonStr[i]):
            vowelBuffer.append(phonStr[i])
        else: # a consonant / punctuation
            newStr.append( ortPhoneGroup("".join(vowelBuffer), ortDict) )
            vowelBuffer.clear()
            newStr.append(ortPhone(phonStr[i], ortDict))
    #flush buffer
    newStr.append( ortPhoneGroup("".join(vowelBuffer), ortDict) )
    return "".join(newStr)

toRem = ["'"]
def cleanMarks(str): #todo: leave marks between vowels.problem: vowels alre also represented by 'r'
    newStr = ""
    for i in range(len(str)):
        if str[i] not in toRem:
            newStr += str[i]
    return newStr
        

def encrypt(englishStr, logs=True):
    # 1) convert to IPA (watch out for digraphs)
    ipaStr = ipa.convert(englishStr)
    #ipaTwoStr = remMarks(ipaStr)
    # 2) shift
    obfStr = obfuscateString(ipaStr, englishLetterTypes)
    # 3) convert to orthography
    finalStr = ortographize(obfStr, orthographyDict)
    if(logs):
        print(ipaStr)
        print(obfStr)
    cleanStr = cleanMarks(finalStr)
    return finalStr

def ptEncrypt(englishStr, logs=True):
    print(encrypt(englishStr, logs))


#todo? function to merge known pronunciations of words and scramble them together into a compound Lojan word. is this desired? e.g. gemfoam
#potential solution: function to replace spaces with ' in the IPA

#test strings
#print(ortographize("aʊeɪoʊɪəaɪɔɪ",orthographyDict))
fable = """
The North Wind and the Sun were disputing which was the stronger, when a traveler came along wrapped in a warm cloak.
They agreed that the one who first succeeded in making the traveler take his cloak off should be considered stronger than the other.
Then the North Wind blew as hard as he could, but the more he blew the more closely did the traveler fold his cloak around him;
and at last the North Wind gave up the attempt. Then the Sun shined out warmly, and immediately the traveler took off his cloak.
And so the North Wind was obliged to confess that the Sun was the stronger of the two.
"""
prayer = """Our Father in heaven,
hallowed be your name.
Your kingdom come.
Your will be done, on earth as it is in heaven.
Give us this day our daily bread.
And forgive us our debts, as we also have forgiven our debtors.
And do not bring us to the time of trial, but rescue us from the evil one.
"""

print("Type in words to translate to Lojanese and press enter:")
while(True):
    ptEncrypt(input())
